filetype plugin on

" show the line number
set nu

" copy to system clipboard 
set clipboard=unnamedplus 

" highlight line and column
" :hi CursorLine   cterm=NONE ctermbg=darkblue ctermfg=white guibg=darkred guifg=white
:hi CursorLine   cterm=NONE ctermbg=darkgrey guibg=darkred guifg=white
:hi CursorColumn cterm=NONE ctermbg=darkgrey ctermfg=white guibg=darkred guifg=white
:nnoremap <Leader>c :set cursorcolumn!<CR>

set cursorline

" highlight search
set hlsearch

" turn on the syntax highlight
syntax on

" indent for C language
set autoindent
set cindent

" show the mode in which the vim is
set showmode

" enable the mouse 
set mouse=a 

" command line height
set cmdheight=2

" activate scroll bar: right and bottom
set guioptions+=rb

" set the width of Taglist window
let Tlist_WinWidth=40

" use F4 to toggle the Taglist window
map <F4> :TlistToggle<cr>

" set the location for the 'tags' file
" for legup
" set tags=/home/liwei/legup-3.0/llvm/tags
" for VAST 
set tags=/home/liwei/VAST/tags

" highlight current word without moving cursor using F9
nnoremap <F9> :let @/='\<<C-R>=expand("<cword>")<CR>\>'<CR>:set hls<CR>
