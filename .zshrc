# Set up the prompt

# autocompletion
autoload -Uz promptinit
promptinit

# use redhat style prompt
prompt redhat
# prompt walters

# liwei customize the prompt
autoload -U colors && colors
PROMPT="%{$fg[red]%}%n%{$reset_color%}@%{$fg[blue]%}%m %{$fg_no_bold[yellow]%}%~ %{$fg[green]%}%# "
# PROMPT="%{$fg[red]%}%n%{$reset_color%}@%{$fg[blue]%}%m %{$fg_no_bold[green]%}%~ %{$reset_color%}%# "
# RPROMPT="[%{$fg_no_bold[yellow]%}%?%{$reset_color%}]"

# source $HOME/.prompt
# redhat_prompt

# to use different colors for files and folders
alias ls='ls --color=auto'
alias l=ls
alias ll='ls -alhF'
alias la='ls -A'
alias grep='grep --color=auto'

setopt histignorealldups sharehistory
# auto completion of command line switches for aliases
setopt completealiases


# Use emacs keybindings even if our EDITOR is set to vi
bindkey -e

# Keep 1000 lines of history within the shell and save it to ~/.zsh_history:
HISTSIZE=1000
SAVEHIST=1000
HISTFILE=~/.zsh_history

# Use modern completion system
autoload -Uz compinit
compinit

zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete _correct _approximate
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' menu select=2
eval "$(dircolors -b)"
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=* l:|=*'
zstyle ':completion:*' menu select=long
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' verbose true

zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'
zstyle ':completion:*:kill:*' command 'ps -u $USER -o pid,%cpu,tty,cputime,cmd'


# liwei begin
# add location for MPI
export PATH=$PATH:/usr/lib/openmpi/include

# add path for VAST
export PATH=$PATH:/home/liwei/VAST/vast-build/bin

# to add in the PATH for llvm
export PATH=$PATH:/home/liwei/legup-3.0/llvm/Release+Asserts/bin
# to add in the PATH for clang
export PATH=$PATH:/home/liwei/clang+llvm-2.9-x86_64-linux/bin

# to add in the PATH for quartus: bin, vsim
export QUARTUS_64BIT=1
export QUARTUS_ROOTDIR=/home/liwei/altera/12.1/quartus/
export PATH=$PATH:/home/liwei/altera/12.1/quartus/bin:/home/liwei/altera/12.1/quartus/sopc_builder/bin
export PATH=$PATH:/home/liwei/Modelsim/modeltech/bin
# export PATH=$PATH:/home/liwei/altera/12.1/quartus/bin:/home/liwei/altera/12.1/quartus/sopc_builder/bin:/home/liwei/altera/12.1/modelsim_ase/linux

# to add in the PATH for Xilinx ISE
export PATH=$PATH:/home/liwei/Xilinx/13.4/ISE_DS:/home/liwei/Xilinx/13.4/ISE_DS/ISE/bin/lin64

# to add in Environment Variable, LM_LICENSE_FILE, for Altera and Xilinx
export LM_LICENSE_FILE=1707@linserv:1708@MapleLicSvr:1717@MapleLicSvr:1707@155.69.100.49

# before to run ise
# source settings64.sh

# liwei end
 
# liwei begin
# to solve messy code in rhythmbox player
# export GST_ID3_TAG_ENCODING=GBK:UTF-8:GB18030
# export GST_ID3V2_TAG_ENCODING=GBK:UTF-8:GB18030

# filtering the commands according to the charactors in the beginning
autoload -Uz up-line-or-beginning-search
autoload -Uz down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search
bindkey '\eOA' up-line-or-beginning-search
bindkey '\e[A' up-line-or-beginning-search
bindkey '\eOB' down-line-or-beginning-search
bindkey '\e[B' down-line-or-beginning-search
# liwei end
 
